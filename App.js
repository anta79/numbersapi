import React,{Component} from 'react'
import {FlatList,ScrollView,View,Text,StyleSheet,TextInput,Button} from 'react-native' 
import RadioForm, {RadioButton,RadioButtonInput,RadioButtonLabel}  from 'react-native-simple-radio-button' ;


var options = [
  {label : "math", value :"math"},
  {label : "date", value :"date"},
  {label : "year", value :"year"},
]

class App extends Component {
   
   state = {
      a_value : "math",
      b_value : "",
      data : [],
      selected : 0 
   }

   render() {

      const renderItem = ({ item }) => (
        <View style={{backgroundColor:"#E1FFBA", margin:4,borderWidth:1,borderColor:"#F30819",width:250,alignItems:"center",padding:5}}>
          <Text style={{fontSize:18}}> {item} </Text>
        </View>
      );

     return (
       <View style={{flex:1 ,backgroundColor : "red"}}>
        <View style={{flex:.2,backgroundColor:"#637BD5",flexDirection:"row",justifyContent:"space-around" ,alignItems:"center"}}>
          <RadioForm  
            radio_props={options} 
            initial={0} 
            onPress={(x)=>this.setState({a_value: x})} 
            buttonSize={10} 
            buttonColor={'white'} 
            selectedButtonColor={'yellow'}
            labelStyle = {{fontSize:12,color:"yellow"}}
           />
           <TextInput
            style={{ height: 40,width:60, borderColor: 'yellow', borderWidth: 2,color:"#A7FCE5",fontSize:19}}
            onChangeText={(text)=>{ this.setState({b_value : text}); }}
            placeholder ="type"
            
           />
           <Button
           title = "find"
           onPress={()=> {
            var que = this.state.b_value 
            var typ = this.state.a_value
             
            var link = 'http://numbersapi.com/'+que+'/'+typ+'?json'
            var res = fetch(link).then(response => response.json()).then((jsondata)=>{ 
                if(jsondata.found) {
                  console.log( jsondata.text)
                  this.state.data.push(jsondata.text)
                  // console.log(this.state.data)
                }
                else {
                  this.state.data.push("Nothing about "+que+" was found")
                }
                this.setState({selected : (this.state.selected + 1)})
            }).catch((err)=> {
              console.err(err);
            })
           }}
           />
        </View>
        <View style={{flex:1,alignItems:"center", backgroundColor:"#FFF32E"}}>
          <FlatList
            style={{width:420}}
            contentContainerStyle={{alignItems:"center"}}
            data={this.state.data}
            renderItem={renderItem}
            extraData = {this.state.selected}
            keyExtractor={(item, index) => 'key'+index}
          />
        </View>
       </View>
     )
   }
}

export default App ;